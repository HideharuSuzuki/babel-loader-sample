
// output.pathに指定するパスがOSによって異なることを防ぐためにpathモジュールを読み込む
const path = require('path');

module.exports = {
    // webpackの動作の種類(development or production or none)
	// development : エラー表示、デバッグしやすいファイルの出力、キャッシュ有効化(再ビルド時間短縮) => 開発時の出力
    // production  : ファイル圧縮、モジュールの最適化 => 本番ファイルの出力
    mode:'development',

    // エントリーポイントの設定
    // エントリーポイント：各モジュールを読み込んで、処理を開始するメインのファイル
    entry: './src/js/app.js',

    // 出力設定
    output:{
        // 出力ファイル名
        filename:'bundle.js',
        // 出力先のパス(絶対パス)
        path:path.resolve(__dirname, 'public/js')
    },

    module:{
        rules:[
            {
                // ローダーの処理対象ファイル(jsファイルを対象とする)
                test: /\.js$/,
                // ローダーの処理対象ディレクトリ
                include:path.resolve(__dirname, 'src/js'),

                use:[
                    {
                        // 利用するローダー
                        loader:'babel-loader',
                        options:{
                            presets:[['@babel/preset-env', { modules: false }]]
                        }
                    }
                ]
            }
        ]
    }
};


